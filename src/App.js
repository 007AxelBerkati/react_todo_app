import React from 'react';
import TodoList from './component/TodoList';
import './App.css';

const App = (props) => {
  console.log(props);
  return (
    <div className="todo-app">
      <TodoList />
    </div>
  );
};

export default App;
